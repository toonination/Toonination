import argparse
import os


parser = argparse.ArgumentParser()
parser.add_argument('--panda3d-dir', default='C:/Users/Parker/Documents/Toon-Island-Aftermath/src/panda',
                    help='The path to the Panda3D build to use for this distribution.')
parser.add_argument('--main-module', default='toontown.toonbase.StartCompiledGame',
                    help='The path to the instantiation module.')
parser.add_argument('modules', nargs='*', default=['otp', 'toontown', 'debug'],
                    help='The Toontown modules to be included in the build.')
args = parser.parse_args()

print 'Building the client...'

os.chdir('build')

cmd = os.path.join(args.panda3d_dir, 'python/ppython.exe')
cmd += ' -m direct.showutil.pfreeze'
cmd += ' -x panda3d'
args.modules.extend(['pandac', 'direct'])
for module in args.modules:
    cmd += ' -i {0}.*.*'.format(module)
cmd += ' -i {0}.*'.format('encodings')
cmd += ' -i {0}.*'.format('panda3d')
cmd += ' -i {0}'.format('base64')
cmd += ' -i {0}'.format('site')
cmd += ' -i {0}'.format('_strptime')
cmd += ' -o TIAEngine.exe'
cmd += ' {0}'.format(args.main_module)

os.system(cmd)
