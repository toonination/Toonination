import argparse
import hashlib
import os
import shutil
import subprocess

from pandac.PandaModules import *

"""
Reads codebase and strips out non-essential files, like server sided code, removes PYC files and clears out debugging tools
"""


# These are arguments that *can* be parsed by running python prepare_client.py -- (args)
parser = argparse.ArgumentParser()
parser.add_argument('--build-dir', default='build',
                    help='The directory in which to store the build files.')
parser.add_argument('--src-dir', default='C:/Users/Parker/Documents/Toon-Island-Aftermath/src',
                    help='The directory of the Toontown source code.')
parser.add_argument('--server-ver', default='Current server version',
                    help='The server version of this build.\n')
parser.add_argument('--resources-dir', default='C:/Users/Parker/Documents/Toon-Island-Aftermath/resources',
                    help='The directory of the Toontown resources.')
parser.add_argument('modules', nargs='*', default=['otp', 'toontown', 'debug'],
                    help='The Toontown modules to be included in the build.')
args = parser.parse_args()

print 'Preparing the client... Please wait'

# Create a clean build directory for us to store our build material:
if not os.path.exists(args.build_dir):
    os.mkdir(args.build_dir)
print 'Build directory = {0}'.format(args.build_dir)

# Set the build version.
serverVersion = args.server_ver
print 'serverVersion = {0}'.format(serverVersion)
print 'Set Client Server Version'

# Copy the provided Toontown modules:

# NonRepeatableRandomSourceUD.py, and NonRepeatableRandomSourceAI.py are
# required to be included. This is because they are explicitly imported by the
# DC file:
print "Including NonRepeatableRandomSourceUD and NonRepeatableRandomSourceAI"
includes = ('NonRepeatableRandomSourceUD.py', 'NonRepeatableRandomSourceAI.py')

# This is a list of explicitly excluded files:
print 'Excluding ServiceStart.py', 'ToontownUberRepository', 'ToontownAIRepository'
excludes = ('ServiceStart.py', 'ToontownUberRepository', 'ToontownAIRepository')

print "Removing _debug_ statements from build"
def minify(f):
    """
    Returns the "minified" file data with removed __debug__ code blocks.
    """

    data = ''

    debugBlock = False  # Marks when we're in a __debug__ code block.
    elseBlock = False  # Marks when we're in an else code block.

    # The number of spaces in which the __debug__ condition is indented:
    indentLevel = 0
    for line in f:
        thisIndentLevel = len(line) - len(line.lstrip())
        if ('if __debug__:' not in line) and (not debugBlock):
            data += line
            continue
        elif 'if __debug__:' in line:
            debugBlock = True
            indentLevel = thisIndentLevel
            continue
        if thisIndentLevel <= indentLevel:
            if 'else' in line:
                elseBlock = True
                continue
            if 'elif' in line:
                line = line[:thisIndentLevel] + line[thisIndentLevel+2:]
            data += line
            debugBlock = False
            elseBlock = False
            indentLevel = 0
            continue
        if elseBlock:
            data += line[4:]

    return data

for module in args.modules:
    print 'Writing modules... And not copying excluded files', module
    for root, folders, files in os.walk(os.path.join(args.src_dir, module)):
        outputDir = root.replace(args.src_dir, args.build_dir)
        if not os.path.exists(outputDir):
            os.mkdir(outputDir)
        for filename in files:
            if filename not in includes:
                if not filename.endswith('.py'):
                    continue
                if filename.endswith('UD.py'):
                    continue
                if filename.endswith('AI.py'):
                    continue
                if filename in excludes:
                    continue
            with open(os.path.join(root, filename), 'r') as f:
                data = minify(f)
            with open(os.path.join(outputDir, filename), 'w') as f:
                f.write(data)

# Let's write _miraidata.py now. _miraidata is a compile-time generated
# collection of data that will be used by the game at runtime. It contains the
# PRC file data, (stripped) DC file, and time zone info.

# First, we need the PRC file data:
print 'Writing public_client.prc into _miraidata'
configFileName = 'general.prc'
configData = []
with open(os.path.join(args.src_dir, 'config', configFileName)) as f:
    data = f.read()
    configData.append(data.replace('SERVER_VERSION', serverVersion))
print 'Using config file: {0}'.format(configFileName)

# Next, we need the (stripped) DC file:
dcFile = DCFile()
filepath = os.path.join(args.src_dir, 'astron/dclass')
for filename in os.listdir(filepath):
    if filename.endswith('.dc'):
        dcFile.read(Filename.fromOsSpecific(os.path.join(filepath, filename)))
dcStream = StringStream()
dcFile.write(dcStream, True)
dcData = dcStream.getData()

# Finally, write our data to _miraidata.py:
print 'Writing _miraidata.py...'
gameData = '''\
CONFIG = %r
DC = %r
'''
with open(os.path.join(args.build_dir, '_miraidata.py'), 'w') as f:
    f.write(gameData % (configData, dcData))

print 'Building multifiles...'
dest = os.path.join(args.build_dir, 'resources')
if not os.path.exists(dest):
    os.mkdir(dest)
dest = os.path.realpath(dest)
os.chdir(args.resources_dir)
for phase in os.listdir('.'):
    if not phase.startswith('phase_'):
        continue
    if not os.path.isdir(phase):
        continue
    filename = phase + '.mf'
    print 'Writing...', filename
    filepath = os.path.join(dest, filename)
    os.system('multify -c -f "%s" "%s"' % (filepath, phase))

print 'Done preparing the client. Run Build_client'
